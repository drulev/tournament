bash:
	docker-compose run --rm web bash

web:
	docker-compose up -d
	docker attach tournament_web_1

deploy:
	docker-compose build
	docker tag tournament-web:latest denisrulev/tournament:latest
	docker push denisrulev/tournament:latest
